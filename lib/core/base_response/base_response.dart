import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(genericArgumentFactories: true)
class BaseResponse<T> extends Equatable {
  final int? code;
  final String? msg;
  final bool? ok;
  final T? obj;
  final String? i18n;

  const BaseResponse({this.code, this.msg, this.ok, this.obj, this.i18n});

  BaseResponse<T> copyWith({
    int? code,
    String? msg,
    bool? ok,
    T? obj,
    String? i18n,
  }) {
    return BaseResponse<T>(
      code: code ?? this.code,
      msg: msg ?? this.msg,
      ok: ok ?? this.ok,
      obj: obj ?? this.obj,
      i18n: i18n ?? this.i18n,
    );
  }

  factory BaseResponse.fromJson(Map<String, dynamic> json, T Function(Object? json) fromJsonT) {
    BaseResponse<T> resultGeneric = BaseResponse<T>(
      code: json['code'] as int? ?? 1,
      msg: json['msg'] as String? ?? "",
      ok: json['ok'] as bool? ?? false,
      i18n: json['i18n'] as String? ?? "",
    );
    if (json['obj'] != null && json['obj'] is Map<String, dynamic>) {
      return resultGeneric.copyWith(
        obj: fromJsonT(json['obj']),
      );
    } else {
      /// cho những TH là kiểu dữ liệu nguyên thủy: String, int, double ...
      return resultGeneric.copyWith(obj: json['obj']);
    }
    return resultGeneric;
  }

  @override
  List<Object?> get props => [code, msg, ok, obj, i18n];
}
