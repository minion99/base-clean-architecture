import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:loading_indicator/loading_indicator.dart';

import 'c_loading_indicator.dart';

class CImage extends StatefulWidget {
  final String? assetsPath;
  final String? assetsNetworkUrl;
  final String? svgPath;
  final String? svgNetworkUrl;
  final double? radius;
  final double? width;
  final double? height;
  final BoxFit? boxFit;
  final Color? color;
  final VoidCallback? onTap;

  const CImage({
    Key? key,
    this.assetsPath,
    this.assetsNetworkUrl,
    this.width,
    this.height,
    this.radius,
    this.boxFit,
    this.color,
    this.svgPath,
    this.svgNetworkUrl,
    this.onTap,
  }) : super(key: key);
  @override
  _CImageState createState() => _CImageState();
}

class _CImageState extends State<CImage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: (() {
        if (kIsWeb) {
          return GestureDetector(
            onTap: widget.onTap,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(widget.radius ?? 0),
                child: Image.asset(
                  widget.assetsPath ?? '',
                  height: widget.height,
                  width: widget.width,
                  fit: widget.boxFit ?? BoxFit.fill,
                )),
          );
        } else {
          if (widget.svgNetworkUrl != null) {
            return CachedNetworkImage(
              width: widget.width,
              height: widget.height,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              imageUrl: widget.svgNetworkUrl ?? '',
              placeholder: (context, url) => const CLoadingIndicator(
                width: 20,
                height: 20,
              ),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            );
          } else if (widget.assetsPath != null) {
            return GestureDetector(
              onTap: widget.onTap,
              child: Image.asset(
                widget.assetsPath ?? '',
                height: widget.height,
                width: widget.width,
                fit: widget.boxFit ?? BoxFit.fill,
              ),
            );
          } else {
            return GestureDetector(
              onTap: widget.onTap,
              child: SvgPicture.asset(
                widget.svgPath ?? '',
                width: widget.width,
                height: widget.height,
                fit: widget.boxFit ?? BoxFit.cover,
                color: widget.color,
              ),
            );
          }
        }
      }()),
    );
  }
}
