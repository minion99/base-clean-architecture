import 'package:flutter/material.dart';

extension ScreenScaleExtension on num {
  double get w {
    final w = MediaQuery.of(AppContext.navigatorKey.currentContext!).size.width;
    return (this / FontSize.SCREEN_WIDTH) * w;
  }

  double get h {
    final h = MediaQuery.of(AppContext.navigatorKey.currentContext!).size.height;
    return (this / FontSize.SCREEN_HEIGHT) * h;
  }
}

extension IntExtensions on int {
  //Screen Utils
  /// Leaves given height of space
  Widget get h => SizedBox(height: toDouble());

  /// Leaves given width of space
  Widget get w => SizedBox(width: toDouble());
}
