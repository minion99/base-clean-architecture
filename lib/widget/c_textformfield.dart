import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CTextFormField extends StatelessWidget {
  final String? hintText;
  final Color? hintColor;
  final bool? obscureText;
  final int? maxLine;
  final List<TextInputFormatter>? textInputFormatter;
  final TextInputType? textInputType;
  final EdgeInsets? contentPadding;
  final double? contentSize;
  final FontWeight? fontWeight;
  final VoidCallback? onComplete;
  final void Function(String)? onchanged;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final TextEditingController? controller;

  const CTextFormField({
    super.key,
    this.controller,
    this.hintText,
    this.onchanged,
    this.suffixIcon,
    this.obscureText,
    this.textInputFormatter,
    this.prefixIcon,
    this.textInputType,
    this.contentPadding,
    this.contentSize,
    this.hintColor,
    this.maxLine,
    this.fontWeight,
    this.onComplete,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: textInputType ?? TextInputType.text,
      inputFormatters: textInputFormatter,
      textInputAction: TextInputAction.next,
      onChanged: onchanged,
      onFieldSubmitted: (_) {},
      obscureText: obscureText ?? false,
      maxLines: maxLine,
      onEditingComplete: onComplete,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
          color: hintColor,
          fontWeight: fontWeight,
          fontSize: contentSize,
          fontFamily: 'OpenSans',
        ),
        isCollapsed: true,
        contentPadding: contentPadding,
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
      ),
    );
  }
}
