import 'package:base_clean_architecture/contants/font_colors.dart';
import 'package:flutter/material.dart';
import 'package:base_clean_architecture/widget/c_text.dart';

class CButton extends StatelessWidget {
  final VoidCallback? onTap;
  final double? height;
  final double? width;
  final Color? backgroundColor;
  final String? title;
  final Color? titleColor;
  final double? borderRadius;

  const CButton({
    Key? key,
    this.onTap,
    this.backgroundColor,
    this.title,
    this.titleColor,
    this.height,
    this.width,
    this.borderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onTap,
      child: Center(
          child: Center(
        child: CText(
          title,
          fontWeight: FontWeight.w600,
          textColor: ColorsApp.color_FFFFFF,
        ),
      )),
    );
  }
}
